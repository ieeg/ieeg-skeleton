<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<link rel="stylesheet" type="text/css" href="simple.css">
<head>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>IEEG Data Test Driver</title>
</head>
<body>
<script src="http://dygraphs.com/dygraph-dev.js"></script>
<h2><img width="70px" src="https://www.ieeg.org/images/IEEGlogo_small.png"/>IEEG Data Test Driver</h2>

<div class="main">
<h4>Please Select a Dataset and position</h4>
<form id="target" action="lookup">
Study name: <input type="text" id="studyName" name="studyName" value="I001_P001_D01"/><br/>
Position (usec offset): <input type="text" id="position" name="position" value="0"/><br/>
<input type="submit" value="Request data">
</form>
</div>

<h4>Snapshots</h4>
<div id="snapshots"></div><br>
<h4>Data</h4>
<div id="visualization" style="width: 600px; height: 300px;"></div><br>
<div id="data" class="vis">
</div>

<script type="text/javascript">
$("#snapshots").load("query?userName=any",function(responseTxt,statusTxt,xhr){
	  
    if(statusTxt=="error")
      alert("Error: "+xhr.status+": "+xhr.statusText);
});
$("#target").submit(function(event){
	event.preventDefault();
    var g = new Dygraph(document.getElementById("visualization"), 
    		"lookup?" + $("#studyName").serialize() + "&" + $("#position").serialize(),
  	        {
  	          customBars: false,
  	          logscale: false
  	        });
	
	  $("#data").load("lookup?" + $("#studyName").serialize() + "&" + $("#position").serialize(),function(responseTxt,statusTxt,xhr){
		  
	    if(statusTxt=="error")
	      alert("Error: "+xhr.status+": "+xhr.statusText);
    
  });
});
</script>

</body>
</html>