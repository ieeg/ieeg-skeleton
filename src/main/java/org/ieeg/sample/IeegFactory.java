package org.ieeg.sample;

import java.net.UnknownHostException;

import edu.upenn.cis.db.mefview.services.TimeSeriesInterface;

/**
 * Factory that returns a singleton for the time series interface
 * 
 * @author zives
 *
 */
public class IeegFactory {
	public static TimeSeriesInterface tsi = null;

	public static synchronized TimeSeriesInterface getInstance() {
		if (tsi == null)
			try {
				
				// TODO: substitute in a legitimate userid, password
				
				tsi = TimeSeriesInterface.newInstance(
					"https://www.ieeg.org/services",
					"userid",
					"password", 
					".");
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return tsi;
	}
}
