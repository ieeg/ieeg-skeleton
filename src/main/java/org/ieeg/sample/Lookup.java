package org.ieeg.sample;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.upenn.cis.db.mefview.services.FilterSpec;
import edu.upenn.cis.db.mefview.services.TimeSeriesDetails;
import edu.upenn.cis.db.mefview.services.TimeSeriesInterface;
import edu.upenn.cis.db.mefview.services.UnscaledTimeSeriesSegment;

/**
 * Servlet implementation for looking up IEEG data
 */
@WebServlet("/lookup")
public class Lookup extends HttpServlet implements Servlet {
	private static final long serialVersionUID = 1L;
	private TimeSeriesInterface tsi;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Lookup() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    @Override
    public void init() {
    	tsi = IeegFactory.getInstance();
    	System.out.println("Done");
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		if (req.getParameter("studyName") == null)
			return;
		
		try {
			System.out.println("Study: " + req.getParameter("studyName"));
			String dataSnapshotID = tsi.getDataSnapshotIdByName(req.getParameter("studyName"));
			
			System.out.println("SS ID: " + dataSnapshotID);
			
			final List<TimeSeriesDetails> datasetTimeSeriesDetails = 
					tsi.getDataSnapshotTimeSeriesDetails(dataSnapshotID);

			final List<String> tsIds = new ArrayList<String>();
			final List<String> labels = new ArrayList<String>();
			final List<FilterSpec> filters = new ArrayList<FilterSpec>();
			for (final TimeSeriesDetails tsDetail : datasetTimeSeriesDetails) {
				tsIds.add(tsDetail.getRevId());
				labels.add(tsDetail.getLabel());
				filters.add(new FilterSpec());
			}
			double startTimeUsec = Double.valueOf(req.getParameter("position"));
			double durationUsec = 1 * 1E6;

			UnscaledTimeSeriesSegment[][] segments = tsi
					.getUnscaledTimeSeriesSet(
							dataSnapshotID,
							tsIds.toArray(new String[0]),
							filters.toArray(new FilterSpec[0]),
							startTimeUsec,
							durationUsec,
							200,
							null);

			StringBuilder b = new StringBuilder();
			b.append("Time");
			for (int i = 0; i < labels.size(); i++)
				b.append("," + labels.get(i));
			b.append("\n");
			
			final List<UnscaledTimeSeriesSegment> tSegmentList = Arrays
					.asList(segments[0]);
			UnscaledTimeSeriesSegment tSegment = tSegmentList.get(0);
			int len = tSegment.getSeriesLength();

			// Time steps
			for (int t= 0; t < len; t++) {
				b.append((long)(t * 1.E6/ datasetTimeSeriesDetails.get(0).getSampleRate()));	
				for (int c = 0; c < labels.size(); c++) {
					b.append(",");
					final List<UnscaledTimeSeriesSegment> segmentList = Arrays
							.asList(segments[c]);
					UnscaledTimeSeriesSegment segment = segmentList.get(0);
					Integer value = segment.getValue(t);
					
					b.append(value);
				}
				b.append("\n");
			}
			resp.setContentType("application/txt");
			resp.setContentLength(b.toString().length());
			resp.getOutputStream().println(b.toString());
			System.out.println(b.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
