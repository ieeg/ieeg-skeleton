package org.ieeg.sample;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.upenn.cis.db.mefview.services.TimeSeriesInterface;

/**
 * Servlet implementation for looking up IEEG data
 */
@WebServlet("/query")
public class Query extends HttpServlet implements Servlet {
	private static final long serialVersionUID = 1L;
	TimeSeriesInterface tsi;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Query() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    @Override
    public void init() {
		tsi = IeegFactory.getInstance();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		if (req.getParameter("userName") == null)
			return;
		
		try {
			System.out.println("Query for data");
			
			Set<String> names = tsi.getOwnedDataSnapshotNames(req.getParameter("userName"));
			
			System.out.println(names);
			
			StringBuilder b = new StringBuilder();
			b.append("<table>");
			for (String name : names)
				b.append("<tr><td>" + name + "</td></tr>");
			b.append("</table>\n");
			
			resp.setContentType("application/txt");
			resp.setContentLength(b.toString().length());
			resp.getOutputStream().println(b.toString());
			System.out.println(b.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
